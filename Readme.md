1. What is Angular?

   Angular is open-source client-side javascript framework for building dynamic web ( and also single page)applications based on MVC architectural pattern.

2. What are the advantages of using Angular?

   * Built-in  template engine for declarative way of data binding and rendering data in mapkup

   * Angularjs create bindings from custom HTML elements attributes (i.e. all binding are carried out in declarative way), which requires less code writing and leads to clean, understandable  and less error-prone code.

   * One-way and two-way data binding for synchronizing data model and DOM

   * MVC architectural pattern on which basis is built angular apps also greatly contributes to a better 
   code quality

   * Whole banch of extended features such as dependency injection, built-in services, routing, animations

   * Directives - special feature that allow extend and manipulates HTML elements 

   * Modules - angular application can consists from multiple modules which allows write more robust and maintainable code by combining closely related application components into separate units.
   
3. What are directives in Angular?

    *Directives* are special markers on HTML elements that signals to angular compiler attach specific behaviour to HTML elements
4. What are controllers in Angular? 

    *Controllers* are one of the building blocks of angularjs. It is a function that is used for augments angular scope (object that refers to model) and serves as connecting link between models and views.

5. What are expressions in Angular? 

    *Expressions* - javascript code snippets that are are mainly placed in interpolation bindings and directive attributes.

6. What are `$scope` and `$rootScope` in Angular? 

    *Scope* in general is object which refers to application model. `$scope` is an object that is accessible from current component e.g Controller, Service only. `$rootScope` refers to an object which is accessible from everywhere of the application.

7. How do we make HTTP get and post calls in Angular?

    HTTP calls in Angular applications are performed using `$http` service. In order to make http call it is necessary use this service with corresponding configuration object that contains request details such as method, url, request payload, headers and so on. Besides this service contains specific shortcut methods for corresponding types of HTTP requests - GET, POST, PUT etc.      
    Example of request with shortcut methods:<br> 
    
    
    GET request :
    ```javascript
    $http.get(<some url>);
    ```
    
	POST request: 
    ```javascript
    $http.post(<some url>, <request data>)
    ```
    Or another syntax: 
    
           
    GET request: 
    ```javascript
    $http({ method: 'GET', url: <some url>})
    ```
	POST request: 
    ```javascript
    $http({method: 'POST', url: <some url>, data: <request data>})
    ```

8. What are services in Angular?

    *Services* are  functions or objects that are available  to other angular components such as controllers and directives through dependency injection. Services can be used for to organize and share code across application and in general serve the same purpose as in other languages and technologies. 

9. What is a Factory in Angular? 

    *Factory* in angular - is method defined on angular module object which used for registering services and takes as arguments service name and function that defines service itself.

10. How to implements validations in Angular? 

    Form validation in Angular can be carried out with the help of standard HTML attributes such as `type` or `required` and also using built-in angular directives such as `ng-required`, `ng-pattern`, `ng-maxlength`, `ng-minlength` which are applied to specific form elements.  
    Angular support for form validation is based on directives that replace the standard HTML elements like `form` and `input`. When Angular encounter form element, it automatically sets up some features for validating form and processes the child elements to look for other elements it needs to process, such as `input` elements.
    The directives that angular uses to replace the standard `form` element define special variables that can be used to check validation state of individual elements or the form as a whole.
    It is important to apply `name` attribute to input elements and form itself in order to access validation information. These are `$pristine`, `$dirty`, `$valid`, `$invalid` and `$error` variables. 
    `$error` variable exposes an object that contains boolean values concerning of specific validation error of *input* element such as `required`, `email` and so on.  
     For example, for element 
     ```HTML
     <form name="myForm">
        <input type="email" required name="email"/>
     </form>
     ```
     access to validation state data can be gained as `myForm.email.$error.required` or `myForm.email.$error.email`.

11. How to implement roiting in Angular?

    Routing in angular can be implemented with the help of standard angular module (`ngRoute`) or using third-party library called angular `ui-router`. The default routing provider (former) has some limitations that make it not very suitable for complex applications. The latter supports everything the normal `ngRoute` can do as well as many extra functions like nested views and multiple named views,strong-type linking between states based on state names.  
    `ngRoute` module allows change application views based on route url whereas `ui-router` uses notion of *states*, which may optionally have routes, as well as other behavior, attached. In order to register routes or states in application it is necessary provide routing configuration in `config()` method of angular module (which executes during application bootstrap). Each of providers provides access to specific services which is used for configuring application routing.  
    Example of configuring routing with `ngRoute`:
    ```javascript
        var app = angular.module("myApp", ["ngRoute"]);

        app.config(function($routeProvider) {
            $routeProvider
            .when("/", {
                templateUrl : "home.html",
                controller: homeController
            })
            .when("/about", {
                templateUrl : "about.html",
                controller: aboutController
            })
            .when("/contacts", {
                templateUrl : "contacts.html",
                controller: contactsController
            });
        });
    ```
    Example of configuring routing with `ui-router`:
    ```javascript
        var app = angular.module("myApp", ["ui.router"]);

        app.config(function($stateProvider) {
            $stateProvider
            .state("home", {
            url:”/”,
                templateUrl : "home.html",
                controller: homeController
            })
            .state("about", {
            url:"/about",
                templateUrl : "about.html",
                controller: aboutController
            })
        .state("contacts", {
            url:"/contacts",
                templateUrl : "contacts.htm",
                controller: contactsController
            })
        });
    ```

12. What are *compile* and *link* phases?

    *Compile* and *link* phases are make up overall angular compilation process (template processing). During compilation process angular parser starts parsing the DOM and whenever the parser encounters a directive it create a function. These functions are termed as *template* or *compiled* functions. In this phase there is no access to the `$scope` data. 
    In the *link* phase the data i.e. `$scope` is attached to the template function and executed to get the final HTML output.